var furnBed =  $('.furn.bed');
var furnSofa =  $('.furn.sofa');
var furnTableRect =  $('.furn.table-rectangle');
var furnTableRo =  $('.furn.table-round');
var furnTv =  $('.furn.tv');
var furnWardrobe =  $('.furn.wardrobe');


function elemListener(elem) {
    elem.addClass('furn_active');
    var activeFurn = $('.furn_active');

    $('.furn_active').append('<button class="rotateBtn"></button><button class="resetBtn"></button>');

    $('.rotateBtn').click(function () {
        if (activeFurn.hasClass('r0')) {
            // console.log('есть r0');
            activeFurn.removeClass('r0 r180 r270');
            activeFurn.addClass('r90');
        } else if (activeFurn.hasClass('r90')) {
            // console.log('есть r90');
            activeFurn.removeClass('r0 r90 r270');
            activeFurn.addClass('r180');
        } else if (activeFurn.hasClass('r180')) {
            // console.log('есть r180');
            activeFurn.removeClass('r0 r90 r180');
            activeFurn.addClass('r270');
        } else {
            // console.log('есть r270. Круг.');
            activeFurn.removeClass('r90 r180 r270');
            activeFurn.addClass('r0');
        }
        activeFurn.css('opacity', '0');
        setTimeout(activeFurn.css('opacity', '1'), 1000);
    });

    $('.resetBtn').click(function () {
        activeFurn.removeAttr("style");
        activeFurn.parent('.furn-wrap').removeClass('bg');
        activeFurn.removeClass('r90 r180 r270');
        activeFurn.addClass('r0');
        activeFurn.draggable({
            containment: false
        });
    });
}


furnBed.mouseenter(function () { elemListener(furnBed);});
furnSofa.mouseenter(function () { elemListener(furnSofa); });
furnTableRect.mouseenter(function () { elemListener(furnTableRect); });
furnTableRo.mouseenter(function () { elemListener(furnTableRo);});
furnTv.mouseenter(function () { elemListener(furnTv);});
furnWardrobe.mouseenter(function () { elemListener(furnWardrobe);});

$('.furn').mouseleave(function () {
    $('.furn').removeClass('furn_active');
    $('.rotateBtn, .resetBtn').remove();
});


//drag & drop
$('.furn').draggable({
    revert: 'invalid',
    cursor: 'move',
    opacity: .9,
    snapTolerance: 15,
    snapMode: 'inner',
    snap: '.plan__rooms'
});

$('.plan__area').droppable({
    drop: function () {
        $('.furn_active').draggable({
            containment: '.plan__area'
        });
        $('.furn_active').parent('.furn-wrap').addClass('bg');
    },
    activeClass: 'active',
    hoverClass: 'hoverUp'
});
