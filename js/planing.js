(() => {
    "use strict";
    let $container = $(".l-flatplan-container:first");
    let $block = $(".b-flatplan:first");

    const fitBgImg = () => {
        $container.removeClass('l-flatplan-container--initialized');
        $block.removeAttr('style');


        let containerHeight = $container.outerHeight() - 256; // with some margin
        let imgHeight = $block.outerHeight();
        if (imgHeight >= containerHeight) {
            $block.width(`${ Math.floor(66 * ((containerHeight) / imgHeight)) }%`);
        }

        $container.addClass('l-flatplan-container--initialized');
    };


    const realFlatWidth = parseInt($(".b-flatplan__pushimg:first").attr('width')) * 3.2;
    const calcFurnRelWidth = (realWidth /*width in cantimeters*/ ) => {
        return (realWidth / realFlatWidth) * 100;
    };


    let inlineStylesForFurns = `
<style>
/*bed*/
.furn.bed.moved, .furn.bed.ui-draggable-dragging {
    width: ${calcFurnRelWidth(160)}%;
}
.furn.bed.rotate.moved,.furn.bed.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(200)}%;
}
/*sofa*/
.furn.sofa.moved,.furn.sofa.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(230)}%;
}
.furn.sofa.rotate.moved,.furn.sofa.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(102)}%;
}
/*corner sofa*/
.furn.sofa_2.moved,.furn.sofa_2.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(230)}%;
}
.furn.sofa_2.rotate.moved,.furn.sofa_2.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(172.5)}%;
}
/*sofa (small)*/
.furn.sofa_3.moved,.furn.sofa_3.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(190)}%;
}
.furn.sofa_3.rotate.moved,.furn.sofa_3.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(90)}%;
}
/*chair*/
.furn.chair.moved,.furn.chair.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(104)}%;
}
.furn.chair.rotate.moved,.furn.chair.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(96)}%;
}
/*wardrobe 1 section*/
.furn.wardrobe_3.moved,.furn.wardrobe_3.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(60)}%;
}
/*wardrobe 2 sections*/
.furn.wardrobe.moved,.furn.wardrobe.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(120)}%;
}
.furn.wardrobe.rotate.moved,.furn.wardrobe.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(60)}%;
}
/*wardrobe 3 sections*/
.furn.wardrobe_2.moved,.furn.wardrobe_2.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(180)}%;
}
.furn.wardrobe_2.rotate.moved,.furn.wardrobe_2.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(60)}%;
}
/*TV*/
.furn.tv.moved,.furn.tv.ui-draggable-dragging  {
   width: ${calcFurnRelWidth(90)}%;
}
.furn.tv.rotate.moved,.furn.tv.rotate.ui-draggable-dragging {
   width: ${calcFurnRelWidth(25)}%;
}
/*table (rectangle)*/
.furn.table-rectangle.moved,.furn.table-rectangle.ui-draggable-dragging{
    width: ${calcFurnRelWidth(120)}%;
}
.furn.table-rectangle.rotate.moved,.furn.table-rectangle.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(110.381)}%;
}
/*papertable*/
.furn.papertable.moved,.furn.papertable.ui-draggable-dragging{
    width: ${calcFurnRelWidth(100)}%;
}
.furn.papertable.rotate.moved,.furn.papertable.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(44)}%;
}
/*bedstand*/
.furn.bedstand.moved,.furn.bedstand.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(52)}%;
}
.furn.bedstand.rotate.moved,.furn.bedstand.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(40)}%;
}
/*kitchenstand (big)*/
.furn.kitchenstand.moved,.furn.kitchenstand.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(80)}%;
}
.furn.kitchenstand.rotate.moved,.furn.kitchenstand.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(60)}%;
}
/*kitchenstand (medium)*/
.furn.kitchenstand_2.moved,.furn.kitchenstand_2.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(60)}%;
}

 /*kitchenstand (small)*/
.furn.kitchenstand_3.moved,.furn.kitchenstand_3.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(40)}%;
}
.furn.kitchenstand_3.rotate.moved,.furn.kitchenstand_3.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(60)}%;
}

/*kitchenslef (big)*/
.furn.kitchenshelf.moved,.furn.kitchenshelf.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(80)}%;
}

.furn.kitchenshelf.rotate.moved,.furn.kitchenshelf.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(40)}%;
}

/*kitchenslef (medium)*/
.furn.kitchenshelf_2.moved,.furn.kitchenshelf_2.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(60)}%;
}

.furn.kitchenshelf_2.rotate.moved,.furn.kitchenshelf_2.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(40)}%;
}

/*kitchenslef (small)*/
.furn.kitchenshelf_3.moved,.furn.kitchenshelf_3.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(40)}%;
}

/*round table*/
.furn.table-round.moved,.furn.table-round.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(60)}%;
}

.furn.table-round.rotate.moved,.furn.table-round.rotate.ui-draggable-dragging {
    width: ${calcFurnRelWidth(75)}%;
}

/*dishwasher*/
.furn.dishwasher.moved,.furn.dishwasher.ui-draggable-dragging  {
    width: ${calcFurnRelWidth(60)}%;
}

</style>
`;

    $(document.head).append(inlineStylesForFurns);

    let $inner = $(".b-flatplan__inner:first");
    $inner.find(".furn").clone().appendTo($inner);
    let $furns = $inner.find(".furn");

    function initDragging($furns) {
        $furns.draggable({
            revert: "invalid",
            snap: ".b-flatplan__area",
            snapMode: "inner",
            snapTolerance: 7,
            start: () => {

                $inner.addClass('b-flatplan__inner--dragging');
            },
            stop: function() {
                let $el = $(this);
                let $parent = $el.parent();
                $el.css("left", parseFloat($el.css("left")) / ($parent.outerWidth() / 100) + "%");
                $el.css("top", parseFloat($el.css("top")) / ($parent.outerHeight() / 100) + "%");
                $inner.removeClass('b-flatplan__inner--dragging');
            },
        });
    }

    function setDraggableElementsInitialPosition() {
        var planImgTopLeftPointCoordinates = $inner.offset();
        $(".furn-cell").each(function(index, el) {
            var $cont = $(el);
            var contCoords = $cont.offset();
            var topOffsetRelatPlan = parseInt(contCoords.top - planImgTopLeftPointCoordinates.top);
            var leftOffsetRelatPlan = parseInt(contCoords.left - planImgTopLeftPointCoordinates.left);
            let opts = $cont.data('container-opts');
            var $furns = $(`.furn.${opts.furn}`);

            let initialPositionStyles;
            if (opts.vOffset) {
                initialPositionStyles = {
                    top: (topOffsetRelatPlan + parseInt(opts.vOffset)) + "px",
                };
            } else {
                initialPositionStyles = {
                    top: (topOffsetRelatPlan) + ($cont.outerHeight() / 2) - parseInt($furns.outerHeight() / 2) + "px",
                };
            }

            initialPositionStyles.left = (leftOffsetRelatPlan) + ($cont.outerWidth() / 2) - parseInt($furns.outerWidth() / 2) + "px";

            $furns.each(function(index, el) {
                let $el = $(el);
                if (!$el.hasClass('moved')) { $el.css(initialPositionStyles); }
            });

            $furns.data('initial-styles', initialPositionStyles);
        });
    }

    initDragging($furns);

    $(".b-flatplan__area").droppable({
        drop: function(event, ui) {
            let $el = $(ui.helper[0]);

            if (!$el.hasClass('moved')) {

                let $clonedFurn = $el.clone();
                $clonedFurn
                    .css($el.data('initial-styles'))
                    .data('initial-styles', $el.data('initial-styles'))
                    .removeClass('ui-draggable-dragging')
                    .appendTo($inner);

                initDragging($clonedFurn);

                setTimeout(() => {
                    $el.addClass('nonAnimatedWidth');
                }, 150);

                $el
                    .addClass('moved')
                    .append('<button class="rotateBtn"></button><button class="resetBtn"></button>');


                let $rotateBtn = $el.find('.rotateBtn');

                $rotateBtn.click(function() {
                    let oldBtnOffset = $rotateBtn.offset();
                    if ($el.hasClass('r0')) {
                        // console.log('есть r0');
                        $el.removeClass('rotate r0 r180 r270');
                        $el.addClass('rotate r90');
                    } else if ($el.hasClass('r90')) {
                        // console.log('есть r90');
                        $el.removeClass('rotate r0 r90 r270');
                        $el.addClass('r180');
                    } else if ($el.hasClass('r180')) {
                        // console.log('есть r180');
                        $el.removeClass('rotate r0 r90 r180');
                        $el.addClass('rotate r270');
                    } else {
                        // console.log('есть r270. Круг.');
                        $el.removeClass('rotate r90 r180 r270');
                        $el.addClass('r0');
                    }

                    let newBtnOffset = $rotateBtn.offset();

                    $el.css({
                        left: parseFloat($el.css("left")) + (oldBtnOffset.left - newBtnOffset.left) + "px",
                        top: parseFloat($el.css("top")) + (oldBtnOffset.top - newBtnOffset.top) + "px",
                    });
                });

                $el.find('.resetBtn').click(function() {
                    $el.remove();
                });
            }
        }
    });

    $(".b-flatplan-buttons__btn--type_reset:first").on('click', (event) => {
        event.preventDefault();
        $inner.find('.moved').remove();
    });

    $(window).on("load resize orientationchange", _.debounce(() => {
        fitBgImg();
        setDraggableElementsInitialPosition();
    }, 100));

})();